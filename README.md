Ce dépôt regroupe les supports pour l'atelier *Statistiques par l'image* de la journée APMEP 2021 en Lorraine.

[![Licence: CC BY-SA 2.0](https://img.shields.io/badge/License-CC%20BY--SA%202.0-lightgrey.svg)](http://creativecommons.org/licenses/by-sa/2.0/fr)

[[_TOC_]] 

# Objectifs
L'**objectif premier** de l'atelier est de présenter un certain nombre de notions statistiques via le traitement et l'analyse de l'image.

Un objectif secondaire est de présenter les notebooks python, ainsi qu'un certain nombre de modules python et de fonctions permettant de faire des calculs statistiques.

Les résultats sont affichés, parfois avec une possibilité d'interaction. Un deuxième objectif secondaire est donc de fournir une introduction à ces fonctionnalités, sur la base d'exemples permettant de débuter sur de bonnes bases l'écriture de ce genre de cellules.

# Supports
Les supports se téléchargent soit à partir de la [page gitlab](https://gitlab.com/ekerrien/apmep2021), soit en récupérant directement [l'archive complète](https://gitlab.com/ekerrien/apmep2021/-/archive/master/apmep2021-master.zip). Les fichiers sont stockés dans un dossier <code>apmep2021-master</code>.

Le support principal est un notebook python qui déroule, cellule par cellule, la présentation de l'atelier. 

Quelques images sont également fournies afin d'expérimenter les notions sur différentes données. Une fonction de lecture d'image est comprise dans la première cellule. Elle gère à la fois les images couleurs et en niveaux de gris. Voici la liste des images:
- une image générique (<code>accueil.jpg</code>) : 

<img src="accueil.jpg" alt="accueil.jpg" height="200">

- une image avec des détails à la fois dans les plages sombres et claires (<code>Photo0185.jpg</code>) :

<img src="Photo0185.jpg" alt="Photo0185.jpg" height="200">

- une image de très faible contraste (<code>aqui.jpg</code>) : 

<img src="aqui.jpg" alt="aqui.jpg" height="200">

- des images permettant de tester des algorithmes de segmentation par binarisation (<code>motifs.jpg</code>, <code>line.jpg</code>, <code>erythrocytes.jpg</code>): 

<img src="motifs.jpg" alt="motifs.jpg" height="200"> <img src="line.jpg" alt="line.jpg" height="200"> <img src="erythrocytes.jpg" alt="erythrocytes.jpg" height="200"> 

La dernière cellule présente l'algorithme de seuillage d'Otsu, et demande de l'implémenter. L'[article original](OTSU_paper.pdf) est fourni pour ceux qui veulent approfondir le sujet.

# Installation
## Version courte
Le code est en python3. Il est rappelé que la version 2 de python n'est plus maintenue depuis janvier 2020 et doit être abandonnée. 

Vous aurez besoin des paquets: <code>ipywidgets</code>, <code>ipympl</code> et <code>scikit-image</code> (ainsi que des paquets dont ils dépendent, notamment <code>matplotlib</code> et <code>numpy</code>).

L'atelier peut se faire sous jupyter notebook ou jupyter lab qui en est une version plus avancée (et plus ergonomique). Jupyter lab requiert cependant quelques étapes d'installation supplémentaires (voir plus bas): installation de Node.js et npm, et installation de l'extension <code>@jupyter-widgets/jupyter-manager</code>, qui peut se faire soit en ligne de commande, soit à partir du gestionnaire d'extensions de jupyter lab.

## Version détaillée
L'installation a été testée sur Linux (Ubuntu 20.04+) et MacOS (BigSur) sous environnement Python/pip, ainsi que sur Windows (10) avec Anaconda. On peut également utiliser l'outil conda au lieu de pip. Les étapes sont très similaires et je ne les détaille donc pas. Voici les étapes d'installation dans ces deux configurations.

## Python/pip
- ouvrir une fenêtre de commande et se déplacer dans le répertoire du dépôt (commande <code>cd</code>)
- créer un environnement virtuel: <code>python3 -m venv ~/APMEP</code>
- activer l'environnement virtuel: <code>. ~/APMEP/bin/activate</code>
- mettre à jour pip: <code>pip install pip --upgrade</code>
- installer les modules: <code>pip install -r requirements.txt</code>
- Pour utiliser **jupyter notebook**: <code>jupyter notebook</code>
- Pour utiliser **jupyter lab**:
    - Installer Node.js et npm (voir plus bas)
    - installer l'extension jupyter lab pour les objets d'interface graphique: <code>jupyter labextension install @jupyter-widgets/jupyterlab-manager</code>
    - lancer jupyter: <code>jupyter lab</code>
- Dans les deux cas, un onglet s'ouvre dans votre navigateur internet par défaut (le navigateur s'ouvre au besoin): aller dans cet onglet, naviguer vers le dossier <code>apmep2021-master</code> et charger le fichier <code>StatImage.ipynb</code>.

## Anaconda
- Dans le cas d'une utilisation de jupyter lab, commencer par installer Node.js et npm (voir ci-dessous).
- [Télécharger](https://www.anaconda.com/products/individual) puis installer Anaconda
- ceux qui connaissent conda peuvent l'utiliser en ligne de commandes pour effectuer les mêmes étapes qu'avec pip. Je présente l'installation via l'interface graphique d'Anaconda Navigator
- Lancer Anaconda Navigator
- Cliquer sur 'Environnements' à gauche, puis sur le bouton 'Create' tout en bas dans le deuxième bandeau à partir de la gauche : renseignez le nom 'APMEP' dans la fenêtre qui s'ouvre, vérifiez que la version de Python est bien 3.x, puis cliquez sur 'Create'
- Patienter...
- Dans l'espace principal, sélectionner 'All' (au lieu de 'Installed') pour visualiser tous les paquets.
- Dans le champ de rechercher, entrer <code>ipywidgets</code>
- Si la liste affichée est vide, cliquer sur 'Update index...'. Normalement le paquet ipywidgets apparaît.
- Sélectionner ipywidgets et cliquer sur 'Apply' en bas à droite. Accepter l'installation de tous les paquets nécessaires.
- Faire de même pour les paquets: <code>ipympl</code> et <code>scikit-image</code>
- Revenir dans 'Home' (bandeau de gauche) qui donne la liste d'applications: jupyter notebook est déjà installé, installer jupyter lab pour l'utiliser
- Lancer jupyter notebook ou jupyter lab
- Naviguer vers le dossier <code>apmep2021-master</code> et charger le fichier <code>StatImage.ipynb</code>

## Etape commune pour utiliser jupyter lab : Installation Node.js et npm
Il est tout à fait possible de faire l'atelier avec jupyter notebook. Cet outil est cependant appelé à être remplacé par jupyter lab qui offre une meilleure ergonomie. Pour pouvoir bénéficier de l'interactivité (widgets), il faut installer une extension, ce qui requiert d'installer Node.js et npm.
- Sur Linux, il suffit de lancer la commande: <code>sudo apt install nodejs npm</code>
- Sur Windows et MacOS, il faut les installer à partir du [site web original](https://nodejs.org/en/download/)
- Dans les deux cas, vous pouvez vérifier que l'installation s'est bien passée en ouvrant un terminal/fenêtre de commande et en entrant les commandes
    - <code>node -v</code>
    - <code>npm -v</code>
